const { ccclass, property } = cc._decorator;

@ccclass
export default class Msg extends cc.Component {
  @property(cc.Label)
  label: cc.Label = null;
  @property(cc.Button)
  button: cc.Button = null;
  @property(cc.Button)
  cancel: cc.Button = null;
}
