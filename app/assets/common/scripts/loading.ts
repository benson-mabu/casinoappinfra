const { ccclass, property } = cc._decorator;

@ccclass
export default class Loading extends cc.Component {
  @property(cc.Label)
  label: cc.Label = null;
  @property(cc.ProgressBar)
  progressBar: cc.ProgressBar = null;
  @property(cc.Label)
  version: cc.Label = null;
  @property(cc.Node)
  display: cc.Node = null;
}
