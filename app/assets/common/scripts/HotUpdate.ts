import { sys } from "./cg";

const { ccclass, property } = cc._decorator;

@ccclass
export default class HotUpdate extends cc.Component implements sys.XHRCallback {
  public bundlePath: string = null;
  public base_url: {
    api: string;
    cdn: string;
    game: string;
  };
  public pathname: string = null;
  public packageName: string = null;
  public storagePath: string = null;
  public active: boolean = null;
  public info: cc.Label = null;
  public fileLabel: cc.Label = null;
  public byteLabel: cc.Label = null;
  public fileProgressBar: cc.ProgressBar = null;
  public byteProgressBar: cc.ProgressBar = null;
  public md5Cache: string = "";

  private url: string = "";
  private xhr: sys.XHR = null;
  private am: jsb.AssetsManager = null;
  private version: string = "";
  private comparison: boolean = false;
  private localStorage: { version: string; load: boolean };

  versionCompareHandle(versionA: string, versionB: string) {
    cc.log(
      `Version Compare: version A is ${versionA}, version B is ${versionB}`
    );
    const vA = versionA.split(".");
    const vB = versionB.split(".");

    for (let i = 0; i < vA.length; ++i) {
      const a = parseInt(vA[i]);
      const b = parseInt(vB[i]);
      if (a !== b) {
        return a - b;
      }
    }

    return vB.length > vA.length ? -1 : 0;
  }

  callback({ info, status }: { info: string; status: boolean }): void {
    cc.log(info);
    this.xhr.removeCallback(this);

    if (status) {
      let obj = JSON.parse(info);

      this.storagePath = jsb.fileUtils.getWritablePath() + this.pathname;
      this.bundlePath = this.storagePath + "/" + this.packageName;
      this.version = obj.version;
      this.md5Cache = obj.md5Cache;
      this.localStorage = JSON.parse(
        cc.sys.localStorage.getItem(this.packageName)
      );

      if (!this.localStorage) this.localStorage = { version: "", load: false };
      if (this.localStorage.version === this.version) {
        jsb.fileUtils.listFiles(this.bundlePath).forEach((event) => {
          let str = event.split("config.");
          if (str.length === 2) {
            str = str[1].split(".");
            if (str.length === 2) this.md5Cache = str[0];
            else this.md5Cache = "";
          }
        });
        if (this.fileProgressBar) this.fileProgressBar.progress = 1;
        if (this.byteProgressBar) this.byteProgressBar.progress = 1;
        this.node.emit(this.pathname, {
          info: "Already up to date with the latest remote version",
          status: true,
        });
        return;
      }

      if (this.comparison) {
        this.comparison = false;
        if (this.fileProgressBar) this.fileProgressBar.progress = 0;
        if (this.byteProgressBar) this.byteProgressBar.progress = 0;
        this.node.emit(this.pathname, {
          info: "New version found, please try to update",
          status: true,
          update: true,
        });
        return;
      }

      if (this.localStorage.load) {
        this.node.emit(this.pathname, {
          info: "New version found, please restart the game and try to update",
          status: false,
          restart: true,
        });
        return;
      }

      if (this.pathname === this.packageName) {
        cc.log(`delete: ${this.storagePath}`);
        jsb.fileUtils.removeDirectory(this.storagePath + "/");
        jsb.fileUtils.removeDirectory(this.storagePath + "_temp/");
      }

      this.am = new jsb.AssetsManager(
        "",
        this.storagePath,
        this.versionCompareHandle
      );

      this.am.setEventCallback(this.checkUpdating.bind(this));
      if (this.am.getState() === jsb.AssetsManager.State.UNINITED) {
        const localFile = `${this.bundlePath}_project.manifest`;
        if (jsb.fileUtils.isFileExist(localFile)) {
          this.am.loadLocalManifest(localFile);
        } else {
          this.am.loadLocalManifest(
            new jsb.Manifest(
              JSON.stringify({
                version: "0.0.0",
                md5Cache: "",
                packageUrl: `${this.url}/${this.pathname}/remote/`,
                remoteManifestUrl: `${this.url}/${this.pathname}/remote/${this.packageName}_project.manifest`,
                remoteVersionUrl: `${this.url}/${this.pathname}/remote/${this.packageName}_version.manifest`,
                assets: {},
                searchPaths: [],
              }),
              this.storagePath
            ),
            this.storagePath
          );

          obj.packageUrl = obj.packageUrl.replace("base_url", this.url);
          obj.remoteManifestUrl = obj.remoteManifestUrl.replace(
            "base_url",
            this.url
          );
          obj.remoteVersionUrl = obj.remoteVersionUrl.replace(
            "base_url",
            this.url
          );
          this.am.loadRemoteManifest(
            new jsb.Manifest(JSON.stringify(obj), null)
          );
        }
      }

      cc.log(`checkUpdate: ${this.storagePath}`);
      this.am.checkUpdate();
      return;
    }
    this.node.emit(this.pathname, { info, status });
  }

  onListen(controllers: any) {
    this.node.on(this.pathname, (event: any) => {
      this.node.off(this.pathname);
      if (this.info) this.info.string = event.info;
      if (event.status) {
        if (event.update) {
          if (controllers.node) controllers.node.active = true;
          return;
        }

        this.active = true;
        return;
      }

      if (controllers.node) {
        controllers.node.active = true;
        controllers.err = true;
        controllers.restart = event.restart;
        controllers.retry = event.retry;
        if (event.url) controllers.url = event.url;
      }
    });
  }

  retry() {
    this.am.downloadFailedAssets();
  }

  downloading(event: any) {
    switch (event.getEventCode()) {
      case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
        cc.log(
          `No local manifest file found, hot update skipped: ${this.packageName}`
        );
        this.am.setEventCallback(null);
        this.node.emit(this.pathname, {
          info: "No local manifest file found, hot update skipped",
          status: false,
        });
        break;
      case jsb.EventAssetsManager.UPDATE_PROGRESSION:
        if (this.fileProgressBar)
          this.fileProgressBar.progress = event.getPercentByFile().toFixed(2);
        if (this.byteProgressBar)
          this.byteProgressBar.progress = event.getPercent().toFixed(2);
        if (this.fileLabel)
          this.fileLabel.string =
            event.getDownloadedFiles() + " / " + event.getTotalFiles();
        if (this.byteLabel)
          this.byteLabel.string =
            event.getDownloadedBytes() + " / " + event.getTotalBytes();
        break;
      case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
      case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
        cc.log(
          `Fail to download manifest file, hot update skipped: ${this.packageName}`
        );
        this.am.setEventCallback(null);
        this.node.emit(this.pathname, {
          info: "Fail to download manifest file, hot update skipped",
          status: false,
        });
        break;
      case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
        cc.log(
          `Already up to date with the latest remote version: ${this.packageName}`
        );
        if (this.fileProgressBar) this.fileProgressBar.progress = 1;
        if (this.byteProgressBar) this.byteProgressBar.progress = 1;
        this.am.setEventCallback(null);
        this.node.emit(this.pathname, {
          info: "Already up to date with the latest remote version",
          status: true,
        });
        break;
      case jsb.EventAssetsManager.UPDATE_FINISHED:
        cc.log(`Update finished: ${this.storagePath}`);
        this.localStorage.version = this.version;
        cc.sys.localStorage.setItem(
          this.packageName,
          JSON.stringify(this.localStorage)
        );
        if (this.fileProgressBar) this.fileProgressBar.progress = 1;
        if (this.byteProgressBar) this.byteProgressBar.progress = 1;
        this.am.setEventCallback(null);
        jsb.fileUtils.renameFile(
          this.storagePath + "/",
          "project.manifest",
          `${this.packageName}_project.manifest`
        );
        this.node.emit(this.pathname, {
          info: "Update finished",
          status: true,
        });
        break;
      case jsb.EventAssetsManager.UPDATE_FAILED:
        cc.log(
          `Update failed: ${this.packageName}, please retry the update after confirming`
        );
        this.node.emit(this.pathname, {
          info: `Update failed: ${this.packageName}, please retry the update after confirming`,
          status: false,
          retry: true,
        });
        break;
      case jsb.EventAssetsManager.ERROR_UPDATING:
        cc.log(
          `Asset update error: ${event.getAssetId()}, ${event.getMessage()}`
        );
        break;
      case jsb.EventAssetsManager.ERROR_DECOMPRESS:
        cc.log(`${event.getMessage()}: ${this.packageName}`);
        break;
      default:
        break;
    }
  }

  hotUpdate() {
    this.am.setEventCallback(this.downloading.bind(this));
    cc.log(`download: ${this.packageName}`);
    this.am.update();
  }

  download() {
    this.xhr = new sys.XHR();
    this.xhr.addCallback(this);
    this.xhr.open(
      "GET",
      `${this.url}/${this.pathname}/remote/${
        this.packageName
      }_project.manifest?dateTime=${new Date().getTime()}`
    );
    this.xhr.send();
  }

  checkUpdating(event: any) {
    switch (event.getEventCode()) {
      case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
        cc.log(
          `No local manifest file found, hot update skipped: ${this.packageName}`
        );
        this.am.setEventCallback(null);
        this.node.emit(this.pathname, {
          info: "No local manifest file found, hot update skipped",
          status: false,
        });
        break;
      case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
      case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
        cc.log(
          `Fail to download manifest file, hot update skipped: ${this.packageName}`
        );
        this.am.setEventCallback(null);
        this.node.emit(this.pathname, {
          info: "Fail to download manifest file, hot update skipped",
          status: false,
        });
        break;
      case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
        cc.log(
          `Already up to date with the latest remote version: ${this.packageName}`
        );
        if (this.fileProgressBar) this.fileProgressBar.progress = 1;
        if (this.byteProgressBar) this.byteProgressBar.progress = 1;
        this.am.setEventCallback(null);
        this.node.emit(this.pathname, {
          info: "Already up to date with the latest remote version",
          status: true,
        });
        break;
      case jsb.EventAssetsManager.NEW_VERSION_FOUND:
        cc.log(`New version found, please try to update: ${this.packageName}`);
        this.hotUpdate();
        break;
      default:
        break;
    }
  }

  checkUpdate() {
    this.comparison = true;

    if (CC_DEV) this.url = "http://localhost";
    else this.url = this.base_url.cdn;

    this.xhr = new sys.XHR();
    this.xhr.addCallback(this);
    this.xhr.open(
      "GET",
      `${this.url}/${this.pathname}/remote/${
        this.packageName
      }_project.manifest?dateTime=${new Date().getTime()}`
    );
    this.xhr.send();
  }
}
