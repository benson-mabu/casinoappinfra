import HotUpdate from "../common/scripts/HotUpdate";
import Loading from "../common/scripts/loading";
import Msg from "../common/scripts/msg";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Init extends cc.Component {
  @property common: cc.AssetManager.Bundle = null;
  @property lobby: cc.AssetManager.Bundle = null;

  private static app: Init;
  private base_url: {
    api: string;
    cdn: string;
    game: string;
  };
  private msg: {
    node: cc.Node;
    script: Msg;
    child: boolean;
    err: boolean;
    restart: boolean;
    url: string;
  } = {
    node: null,
    script: undefined,
    child: false,
    err: false,
    restart: false,
    url: null,
  };
  private loading: {
    node: cc.Node;
    script: Loading;
  } = {
    node: null,
    script: undefined,
  };
  private hotupdate: {
    script: HotUpdate;
  } = {
    script: undefined,
  };
  private language: string;

  reboot() {
    if (this.msg.err) {
      this.msg.script.button.node.off("click", this.reboot, this);
    }

    if (this.common) {
      this.common.releaseAll();
      cc.assetManager.removeBundle(this.common);
    }

    if (this.lobby) {
      this.lobby.releaseAll();
      cc.assetManager.removeBundle(this.lobby);
    }

    if (!this.msg.url) cc.game.restart();

    cc.sys.openURL(this.msg.url);
    window.close();
  }

  chechUpdate() {
    this.hotupdate.script.pathname = "lobby";
    this.hotupdate.script.packageName = this.hotupdate.script.pathname;
    this.hotupdate.script.base_url = this.base_url;
    this.hotupdate.script.info = this.msg.script.label;
    this.hotupdate.script.fileLabel = this.loading.script.label;
    this.hotupdate.script.fileProgressBar = this.loading.script.progressBar;
    this.msg.node.active = false;
    this.msg.child = true;
    this.hotupdate.script.onListen(this.msg);
    this.hotupdate.script.checkUpdate();
  }

  cancel() {
    this.msg.script.cancel.node.off("click", this.cancel, this);
    this.msg.script.button.node.off("click", this.reboot, this);
    this.msg.script.cancel.node.active = false;
    this.msg.err = false;
    this.node.removeChild(this.msg.node);

    this.chechUpdate();
  }

  public static remoteConfigCallback(status: boolean) {
    if (Init.app.language.substr(0, 2) == "zh") Init.app.language = "cn";
    else Init.app.language = "en";
    if (!status) {
      Init.app.msg.child = true;
      Init.app.msg.node.active = true;
      Init.app.msg.err = true;
      Init.app.msg.script.label.string =
        "Unable to obtain remote config, please check the network connection";
      Init.app.msg.script.button.node.active = false;
      return;
    }

    let base_url: string;
    let app_updeta_url: string;
    let compatibility_versions: any;
    let latest_version: string;
    let version: string;
    if (cc.sys.os === cc.sys.OS_ANDROID) {
      base_url = jsb.reflection.callStaticMethod(
        "cg/FirebaseSDK",
        "getRemoteConfig",
        "(Ljava/lang/String;)Ljava/lang/String;",
        "env_base_url_preDev"
      );
      app_updeta_url = jsb.reflection.callStaticMethod(
        "cg/FirebaseSDK",
        "getRemoteConfig",
        "(Ljava/lang/String;)Ljava/lang/String;",
        "app_updeta_url"
      );
      compatibility_versions = jsb.reflection.callStaticMethod(
        "cg/FirebaseSDK",
        "getRemoteConfig",
        "(Ljava/lang/String;)Ljava/lang/String;",
        "compatibility_versions"
      );
      latest_version = jsb.reflection.callStaticMethod(
        "cg/FirebaseSDK",
        "getRemoteConfig",
        "(Ljava/lang/String;)Ljava/lang/String;",
        "latest_version"
      );
      version = jsb.reflection.callStaticMethod(
        "org/cocos2dx/lib/Cocos2dxHelper",
        "getVersion",
        "()Ljava/lang/String;"
      );
      jsb.reflection.callStaticMethod(
        "org/cocos2dx/lib/Cocos2dxHelper",
        "setKeepScreenOn",
        "(Z)V",
        true
      );
    } else if (cc.sys.os === cc.sys.OS_IOS) {
      base_url = jsb.reflection.callStaticMethod(
        "FirebaseSDK",
        "getRemoteConfig:",
        "env_base_url_preDev"
      );
      app_updeta_url = jsb.reflection.callStaticMethod(
        "FirebaseSDK",
        "getRemoteConfig:",
        "app_updeta_url"
      );
      compatibility_versions = jsb.reflection.callStaticMethod(
        "FirebaseSDK",
        "getRemoteConfig:",
        "compatibility_versions"
      );
      latest_version = jsb.reflection.callStaticMethod(
        "FirebaseSDK",
        "getRemoteConfig:",
        "latest_version"
      );
      version = jsb.reflection.callStaticMethod("DeviceSDK", "getVersion");
      jsb.reflection.callStaticMethod("DeviceSDK", "setKeepScreenOn:", true);
    } else {
      base_url =
        '{\
            "api":"https://api.cggame.net",\
            "cdn":"https://cdn.cggame.net",\
            "game":"https://game.cggame.net"\
          }';
      app_updeta_url = "https://game.cggame.net/remote/install.html";
      latest_version = "0.1.14";
      version = "0.1.14";
    }

    Init.app.base_url = JSON.parse(base_url);
    let localStorage = JSON.parse(cc.sys.localStorage.getItem("init"));
    if (!localStorage) localStorage = {};
    if (localStorage.base_url !== Init.app.base_url)
      localStorage.base_url = Init.app.base_url;
    if (!localStorage.language) localStorage.language = Init.app.language;
    cc.sys.localStorage.setItem("init", JSON.stringify(localStorage));

    Init.app.loading.script.version.string = version;
    if (version != latest_version) {
      let checkLocalVersionCompatibility: boolean = false;
      let checkRemoteVersionCompatibility: boolean = false;
      compatibility_versions = JSON.parse(compatibility_versions);
      compatibility_versions.forEach((value: string) => {
        if (version == value) checkLocalVersionCompatibility = true;
        if (latest_version == value) checkRemoteVersionCompatibility = true;
      });
      if (checkLocalVersionCompatibility && checkRemoteVersionCompatibility)
        Init.app.msg.script.cancel.node.active = true;

      Init.app.msg.child = true;
      Init.app.msg.node.active = true;
      Init.app.msg.err = true;
      Init.app.msg.script.label.string =
        "The version is too old, please update the app";
      Init.app.msg.url = app_updeta_url;
    } else {
      Init.app.chechUpdate();
    }
  }

  onLoad() {
    Init.app = this;

    if (CC_DEV) cc.sys.localStorage.clear();
    else {
      if (cc.sys.os === cc.sys.OS_ANDROID) {
        jsb.reflection.callStaticMethod(
          "cg/DeviceSDK",
          "setOrientation",
          "(I)V",
          1
        );
      } else if (cc.sys.os === cc.sys.OS_IOS) {
        jsb.reflection.callStaticMethod("DeviceSDK", "setOrientation:", 1);
      }
    }

    let tmp = {};
    let n = cc.sys.localStorage.length;
    while (n) {
      for (let i = 0; i < cc.sys.localStorage.length; i++) {
        const key = cc.sys.localStorage.key(i);
        if (!tmp[key]) {
          tmp[key] = key;
          n--;
          try {
            while (1) {
              let localStorage = JSON.parse(cc.sys.localStorage.getItem(key));
              if (localStorage.load) {
                localStorage.load = false;
                cc.sys.localStorage.setItem(key, JSON.stringify(localStorage));
              }
              cc.log(`key:${key} value:${cc.sys.localStorage.getItem(key)}`);
              if (!JSON.parse(cc.sys.localStorage.getItem(key)).load) break;
            }
          } catch (e) {
            cc.log(e);
          }
        }
      }
    }

    const listen: string = "bundle";
    let bundleLoadCompletedCount: number = 0;

    this.node.on(listen, (reboot: boolean) => {
      if (reboot) {
        this.node.off(listen);
        this.reboot();
        return;
      }
      bundleLoadCompletedCount++;
      if (bundleLoadCompletedCount === 3) {
        this.node.off(listen);

        if (cc.sys.os === cc.sys.OS_ANDROID) {
          this.language = jsb.reflection.callStaticMethod(
            "org/cocos2dx/lib/Cocos2dxHelper",
            "getCurrentLanguageCode",
            "()Ljava/lang/String;"
          );
          jsb.reflection.callStaticMethod(
            "cg/FirebaseSDK",
            "remoteConfigFetchAndActivate",
            "()V"
          );
        } else if (cc.sys.os === cc.sys.OS_IOS) {
          this.language = jsb.reflection.callStaticMethod(
            "DeviceSDK",
            "getCurrentLanguageCode"
          );
          jsb.reflection.callStaticMethod(
            "FirebaseSDK",
            "remoteConfigFetchAndActivate"
          );
        } else {
          this.language = "zh";
          Init.remoteConfigCallback(true);
        }
      }
    });

    cc.assetManager.loadBundle(
      "common",
      (err: Error, bundle: cc.AssetManager.Bundle) => {
        if (err) {
          this.node.emit(listen, true);
          return;
        }

        this.common = bundle;

        bundle.load("prefabs/msg", (err: Error, assets: cc.Asset) => {
          if (err) {
            this.node.emit(listen, true);
            return;
          }
          this.msg.node = cc.instantiate(<cc.Node>(<unknown>assets));
          this.msg.script = this.msg.node.getComponent("msg");
          this.node.emit(listen, false);
        });

        bundle.load("prefabs/loading", (err: Error, assets: cc.Asset) => {
          if (err) {
            this.node.emit(listen, true);
            return;
          }
          this.loading.node = cc.instantiate(<cc.Node>(<unknown>assets));
          this.loading.script = this.loading.node.getComponent("loading");
          this.loading.script.display.active = false;
          this.node.addChild(this.loading.node);
          this.node.emit(listen, false);
        });

        bundle.load("prefabs/HotUpdate", (err: Error, assets: cc.Asset) => {
          if (err) {
            this.node.emit(listen, true);
            return;
          }
          this.hotupdate.script = cc
            .instantiate(<cc.Node>(<unknown>assets))
            .getComponent("HotUpdate");
          this.node.emit(listen, false);
        });
      }
    );
  }

  download() {
    this.msg.script.button.node.off("click", this.download, this);
    this.node.removeChild(this.msg.node);

    this.loading.script.display.active = true;
    this.msg.node.active = false;
    this.msg.child = true;
    this.hotupdate.script.onListen(this.msg);
    this.hotupdate.script.download();
  }

  update() {
    if (this.hotupdate.script) {
      if (this.msg.node.active) {
        if (this.msg.child) {
          this.msg.child = false;
          this.node.addChild(this.msg.node);
          if (this.msg.err) {
            this.msg.script.button.node.on("click", this.reboot, this);
            if (this.msg.script.cancel.node.active)
              this.msg.script.cancel.node.on("click", this.cancel, this);
            return;
          }
          this.msg.script.button.node.on("click", this.download, this);
        }
      }

      if (this.hotupdate.script.active) {
        this.hotupdate.script.active = false;
        if (this.loading.script.display.active) {
          this.loading.script.display.active = false;
        }

        cc.assetManager.loadBundle(
          this.hotupdate.script.bundlePath,
          {
            version: this.hotupdate.script.md5Cache,
            reload: true,
            cacheAsset: false,
            cacheEnabled: true,
          },
          (err: Error, bundle: cc.AssetManager.Bundle) => {
            if (err) {
              this.msg.node.active = true;
              this.msg.err = true;
              this.msg.script.label.string =
                "error loadbundle: " + this.hotupdate.script.pathname;
              return;
            }

            this.lobby = bundle;

            bundle.loadScene(
              "login",
              (err: Error, sceneAsset: cc.SceneAsset) => {
                if (err) {
                  this.msg.node.active = true;
                  this.msg.err = true;
                  this.msg.script.label.string =
                    "error bundle.loadscene: login";
                  return;
                }
                cc.director.runScene(sceneAsset);
              }
            );
          }
        );
      }
    }
  }
}

if (!window.globalThis["cg"]) window.globalThis["cg"] = {};
window.globalThis["cg"]["init"] = Init;
