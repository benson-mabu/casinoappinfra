var fs = require('fs');
var path = require('path');
var crypto = require('crypto');

var manifest = {
	version: '',
	md5Cache: '',
    packageUrl: '',
    remoteManifestUrl: '',
    remoteVersionUrl: '',
    assets: {},
    searchPaths: []
};
var languagePackage = ['','_cn','_en'];

var url = '';
var dest = '';
var src = '';
var name = '';

// Parse arguments
var i = 2;
while ( i < process.argv.length) {
    var arg = process.argv[i];
	
    switch (arg) {
    case '--url' :
    case '-u' :
        url = process.argv[i+1];
        i += 2;
        break;
    case '--version' :
    case '-v' :
        manifest.version = process.argv[i+1];
        i += 2;
        break;
    case '--src' :
    case '-s' :
        src = process.argv[i+1];
        i += 2;
        break;
    case '--dest' :
    case '-d' :
        dest = process.argv[i+1];
        i += 2;
        break;
	case '--name' :	
	case '-n' :
	    name = process.argv[i+1];
		i += 2;
	    break;
    default :
        i++;
        break;
    }
}

function readDir (dir, obj) {
    var stat = fs.statSync(dir);
    if (!stat.isDirectory()) {
        return;
    }
    var subpaths = fs.readdirSync(dir), subpath, size, md5, compressed, relative, str;
    for (var i = 0; i < subpaths.length; ++i) {
        if (subpaths[i][0] === '.') {
            continue;
        }
        subpath = path.join(dir, subpaths[i]);
		str = subpath.split('.');
		if (str[0].indexOf('config') != -1 && str.length == 3) manifest.md5Cache = str[1];
        stat = fs.statSync(subpath);
        if (stat.isDirectory()) {
            readDir(subpath, obj);
        }
        else if (stat.isFile()) {
            // Size in Bytes
            size = stat['size'];
            md5 = crypto.createHash('md5').update(fs.readFileSync(subpath)).digest('hex');
            compressed = path.extname(subpath).toLowerCase() === '.zip';

            relative = path.relative(src, subpath);
            relative = relative.replace(/\\/g, '/');
            relative = encodeURI(relative);
            obj[relative] = {
                'size' : size,
                'md5' : md5
            };
            if (compressed) {
                obj[relative].compressed = true;
            }
        }
    }
}

var mkdirSync = function (path) {
    try {
        fs.mkdirSync(path);
    } catch(e) {
        if ( e.code != 'EEXIST' ) throw e;
    }
}

languagePackage.forEach((language)=>{
	manifest.packageUrl = url;
	manifest.remoteManifestUrl = url + `${name}${language}_project.manifest`;
	manifest.remoteVersionUrl = url + `${name}${language}_version.manifest`;
	manifest.assets = {};
	manifest.searchPaths = [];
	try {
		readDir(path.join(src, `${name}${language}`), manifest.assets);
		mkdirSync(dest);

		fs.writeFile(path.join(dest, `${name}${language}_project.manifest`), JSON.stringify(manifest), (err) => {
			if (err) throw err;
			console.log(`${name}${language} Manifest successfully generated`);
		});

		delete manifest.assets;
		delete manifest.searchPaths;
		
		fs.writeFile(path.join(dest, `${name}${language}_version.manifest`), JSON.stringify(manifest), (err) => {
			if (err) throw err;
			console.log(`${name}${language} Version successfully generated`);
		});
	} catch(e){
		console.log(JSON.stringify(e));
	}
});