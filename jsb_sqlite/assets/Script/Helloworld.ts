const { ccclass, property } = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {
  @property(cc.Label)
  label: cc.Label = null;

  @property
  text: string = "hello";

  @property(cc.Button)
  btnAdd: cc.Button = null;

  @property(cc.Button)
  btnQuery: cc.Button = null;

  sqlite: cg.SQLite = null;

  onLoad() {
    if (this.btnAdd) {
      this.btnAdd.node.on("click", this.onBtnAddClick, this);
    }
    if (this.btnQuery) {
      this.btnQuery.node.on("click", this.onBtnQueryClick, this);
    }
  }

  onDestroy() {
    if (this.sqlite) {
      this.sqlite.close();
    }
  }
  start() {
    // init logic
    this.label.string = this.text;

    try {
      const { SQLite } = cg;
      this.label.string = "new SQLite";
      this.sqlite = new SQLite("db001");
      cc.log("create table");
      let sql = `CREATE TABLE IF NOT EXISTS JSB_SQL (id INTEGER PRIMARY KEY AUTOINCREMENT, col1 text, col2 text);`;
      let result = this.sqlite.exec(sql, (code, msg) => {
        cc.log(code, msg);
      });
      if (result < 0) {
        cc.error(`Create table failed, result: ${result}`);
        throw new Error("Create table failed");
      }
      this.label.string = "new SQLite Test OK";
    } catch (error) {
      this.label.string = error.message;
    }
  }

  private onBtnAddClick() {
    cc.log("onBtnAddClick insert data");

    try {
      const sql = `INSERT INTO JSB_SQL (col1,col2) VALUES ('ABC','DEF');`;
      let result = this.sqlite.exec(sql, (code, msg) => {
        cc.log(code, msg);
      });
      if (result < 0) {
        cc.error(`INSERT table failed, result: ${result}`);
        throw new Error("INSERT table failed");
      }
      this.label.string = "sqlite insert data OK";
    } catch (error) {
      this.label.string = error.message;
    }
  }

  private onBtnQueryClick() {
    cc.log("onBtnQueryClick query data");
    let info = "";
    try {
      const sql = `SELECT json_group_array(
        json_object('id',id,'col1',col1,'col2',col2)
      ) AS json_result FROM (SELECT * FROM JSB_SQL);`;
      let result = this.sqlite.query(sql, (code, msg, dataString) => {
        try {
          cc.log(code, msg, dataString);
          const data = JSON.parse(dataString);
          info = JSON.stringify({ code, msg, data });
        } catch (error) {
          cc.error(`query failed, result: ${error.message}`);
          info = error.message;
        }
      });
      if (result < 0) {
        cc.error(`query table failed, result: ${result}`);
        throw new Error("query table failed");
      }
      info += "\n sqlite query data OK";
      this.label.string = info;
    } catch (error) {
      this.label.string = error.message;
    }
  }
}
