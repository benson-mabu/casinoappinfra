declare namespace cg {
  export class SQLite {
    constructor(database: string);
    exec(sql: string, callback: function): int;
    query(sql: string, callback: function): int;
    close(): int;
  }
}
