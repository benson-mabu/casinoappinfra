"use strict";
cc._RF.push(module, 'e1b90/rohdEk4SdmmEZANaD', 'Helloworld');
// Script/Helloworld.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Helloworld = /** @class */ (function (_super) {
    __extends(Helloworld, _super);
    function Helloworld() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.label = null;
        _this.text = "hello";
        _this.btnAdd = null;
        _this.btnQuery = null;
        _this.sqlite = null;
        return _this;
    }
    Helloworld.prototype.onLoad = function () {
        if (this.btnAdd) {
            this.btnAdd.node.on("click", this.onBtnAddClick, this);
        }
        if (this.btnQuery) {
            this.btnQuery.node.on("click", this.onBtnQueryClick, this);
        }
    };
    Helloworld.prototype.onDestroy = function () {
        if (this.sqlite) {
            this.sqlite.close();
        }
    };
    Helloworld.prototype.start = function () {
        // init logic
        this.label.string = this.text;
        try {
            var SQLite = cg.SQLite;
            this.label.string = "new SQLite";
            this.sqlite = new SQLite("db001");
            cc.log("create table");
            var sql = "CREATE TABLE IF NOT EXISTS JSB_SQL (id INTEGER PRIMARY KEY AUTOINCREMENT, col1 text, col2 text);";
            var result = this.sqlite.exec(sql, function (code, msg) {
                cc.log(code, msg);
            });
            if (result < 0) {
                cc.error("Create table failed, result: " + result);
                throw new Error("Create table failed");
            }
            this.label.string = "new SQLite Test OK";
        }
        catch (error) {
            this.label.string = error.message;
        }
    };
    Helloworld.prototype.onBtnAddClick = function () {
        cc.log("onBtnAddClick insert data");
        try {
            var sql = "INSERT INTO JSB_SQL (col1,col2) VALUES ('ABC','DEF');";
            var result = this.sqlite.exec(sql, function (code, msg) {
                cc.log(code, msg);
            });
            if (result < 0) {
                cc.error("INSERT table failed, result: " + result);
                throw new Error("INSERT table failed");
            }
            this.label.string = "sqlite insert data OK";
        }
        catch (error) {
            this.label.string = error.message;
        }
    };
    Helloworld.prototype.onBtnQueryClick = function () {
        cc.log("onBtnQueryClick query data");
        var info = "";
        try {
            var sql = "SELECT json_group_array(\n        json_object('id',id,'col1',col1,'col2',col2)\n      ) AS json_result FROM (SELECT * FROM JSB_SQL);";
            var result = this.sqlite.query(sql, function (code, msg, dataString) {
                try {
                    cc.log(code, msg, dataString);
                    var data = JSON.parse(dataString);
                    info = JSON.stringify({ code: code, msg: msg, data: data });
                }
                catch (error) {
                    cc.error("query failed, result: " + error.message);
                    info = error.message;
                }
            });
            if (result < 0) {
                cc.error("query table failed, result: " + result);
                throw new Error("query table failed");
            }
            info += "\n sqlite query data OK";
            this.label.string = info;
        }
        catch (error) {
            this.label.string = error.message;
        }
    };
    __decorate([
        property(cc.Label)
    ], Helloworld.prototype, "label", void 0);
    __decorate([
        property
    ], Helloworld.prototype, "text", void 0);
    __decorate([
        property(cc.Button)
    ], Helloworld.prototype, "btnAdd", void 0);
    __decorate([
        property(cc.Button)
    ], Helloworld.prototype, "btnQuery", void 0);
    Helloworld = __decorate([
        ccclass
    ], Helloworld);
    return Helloworld;
}(cc.Component));
exports.default = Helloworld;

cc._RF.pop();