// export namespace sys {
export function URL(url: string): { [string: string]: string } {
  let params: { [string: string]: string } = {};
  url
    .split("?")[1]
    .split("&")
    .forEach((event) => {
      const [key, value] = event.split("=");
      params[decodeURIComponent(key)] = decodeURIComponent(value);
    });
  return params;
}
// }
