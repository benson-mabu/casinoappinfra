// export namespace sys {
export interface XHRCallback {
  callback({ info: string, status: boolean }): void;
}

export class XHR {
  xhr: XMLHttpRequest;
  method: string;
  callbackList: Set<XHRCallback> = new Set();
  constructor() {
    this.xhr = new XMLHttpRequest();
    this.xhr.onreadystatechange = () => {
      if (this.xhr.readyState === 4) {
        if (this.xhr.status === 200) {
          this.callback({
            info: this.xhr.response,
            status: true,
          });
        } else {
          this.callback({
            info: `xhr error: ${this.xhr.status.toString()}`,
            status: false,
          });
        }
      }
    };

    this.xhr.onerror = (event) => {
      this.callback({
        info: `xhr error: ${JSON.stringify(event)}`,
        status: false,
      });
    };

    this.xhr.ontimeout = () => {
      this.callback({
        info: `xhr error: timeout`,
        status: false,
      });
    };
  }

  // 加入 send 結果 callback
  addCallback(callback: XHRCallback): void {
    this.callbackList.add(callback);
  }

  // 移除 send 結果 callback
  removeCallback(callback: XHRCallback): void {
    this.callbackList.delete(callback);
  }

  // open http url for request
  open(
    method: string,
    url: string,
    async: boolean = true,
    timeout: number = 2000
  ): void {
    this.xhr.open(method, url, async);
    this.xhr.timeout = timeout;
    this.method = method;
  }

  // send data
  send(data?: Object) {
    const xhr = this.xhr;
    if (this.method === "POST")
      xhr.setRequestHeader("content-type", "application/json");
    if (data) xhr.send(JSON.stringify(data));
    else xhr.send();
  }

  private callback({ info, status }: { info: string; status: boolean }): void {
    this.callbackList.forEach((cb) => {
      cb.callback({ info, status });
    });
    return;
  }
}
// }
