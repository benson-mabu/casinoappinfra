import { sys } from "./cg/index";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Login extends cc.Component implements sys.XHRCallback {
  @property(cc.Label)
  pName: cc.Label = null;
  @property(cc.Label)
  status: cc.Label = null;
  @property(cc.Button)
  gButton: cc.Button = null;
  @property(cc.Button)
  fButton: cc.Button = null;

  private static app: Login;
  private xhr: sys.XHR;
  private Authorization: string;
  private token: string;

  onSignIn() {
    this.gButton.node.on("click", this.onGoogleSignIn, this);
    this.fButton.node.on("click", this.onFacebookSignIn, this);
    this.gButton.node.active = true;
    this.fButton.node.active = true;

    this.status.string = "sign in";
    this.pName.string = "";
  }

  onLoad() {
    Login.app = this;
    this.onSignIn();
  }

  onFacebookSignIn() {
    this.status.string = "";
    this.gButton.node.off("click", this.onGoogleSignIn, this);
    this.fButton.node.off("click", this.onFacebookSignIn, this);
    this.gButton.node.active = false;

    if (cc.sys.os === cc.sys.OS_ANDROID) {
      jsb.reflection.callStaticMethod("cg/FacebookSDK", "signIn", "()V");
    } else if (cc.sys.os === cc.sys.OS_IOS) {
      jsb.reflection.callStaticMethod("FacebookSDK", "signIn");
    }
  }

  onGoogleSignIn() {
    this.status.string = "";
    this.gButton.node.off("click", this.onGoogleSignIn, this);
    this.fButton.node.off("click", this.onFacebookSignIn, this);
    this.fButton.node.active = false;

    if (cc.sys.os === cc.sys.OS_ANDROID) {
      jsb.reflection.callStaticMethod("cg/GoogleSDK", "signIn", "()V");
    } else if (cc.sys.os === cc.sys.OS_IOS) {
      jsb.reflection.callStaticMethod("GoogleSDK", "signIn");
    }
  }

  onSignOut() {
    this.status.string = "";
    this.gButton.node.off("click", this.onSignOut, this);
    this.fButton.node.off("click", this.onSignOut, this);

    this.xhr = new sys.XHR();
    this.xhr.addCallback(this);
    this.xhr.open(
      "POST",
      "https://game.cggame.net/ClientGateway/api/action/logoutAuth"
    );
    this.xhr.send({
      query: JSON.stringify({
        agentID: "fakefake.singlewallet",
        Authorization: this.Authorization,
        authToken: this.token,
      }),
      Authorization: this.Authorization,
    });
  }

  callback({ info, status }: { info: string; status: boolean }): void {
    cc.log(info);
    this.xhr.removeCallback(this);

    if (!status) {
      this.onSignIn();
      return;
    }

    const obj = JSON.parse(info);
    if (!obj.result) {
      this.onSignIn();
      return;
    }

    if (obj.data.status === 1) {
      this.Authorization = obj.data.Authorization;
      this.pName.string = obj.data.memberID;
      this.status.string = "sign out";
      this.gButton.node.on("click", this.onSignOut, this);
      this.fButton.node.on("click", this.onSignOut, this);
    } else {
      if (cc.sys.os === cc.sys.OS_ANDROID) {
        if (this.gButton.node.active) {
          jsb.reflection.callStaticMethod("cg/GoogleSDK", "signOut", "()V");
        }
        if (this.fButton.node.active) {
          jsb.reflection.callStaticMethod("cg/FacebookSDK", "signOut", "()V");
        }
      } else if (cc.sys.os === cc.sys.OS_IOS) {
        if (this.gButton.node.active) {
          jsb.reflection.callStaticMethod("GoogleSDK", "signOut");
        }
        if (this.fButton.node.active) {
          jsb.reflection.callStaticMethod("FacebookSDK", "signOut");
        }
      }
      this.onSignIn();
    }
  }

  public static signInCallback(token) {
    if (!token) {
      Login.app.onSignIn();
      return;
    }

    cc.log(token);
    Login.app.token = token;

    Login.app.xhr = new sys.XHR();
    Login.app.xhr.addCallback(Login.app);
    Login.app.xhr.open(
      "POST",
      "https://game.cggame.net/ClientGateway/api/loginAuth"
    );
    Login.app.xhr.send({
      isAnonymous: false,
      agentID: "fakefake.singlewallet",
      authToken: token,
    });
  }
}

window.globalThis["cg"] = Login;
