#!/bin/bash

QC=1

declare -A a000=([name]='lobby-h5' [git]='https://gitlab.com/championgameteam/production/game/lobby/lobby-h5-new.git' [branch]='master')
declare -A a001=([version]='0.1.56' [name]='lobby' [git]='https://gitlab.com/championgameteam/production/game/lobby/lobby-app.git' [branch]='master')
declare -A a02=([version]='0.1.17' [name]='0002' [git]='https://gitlab.com/championgameteam/production/game/godoffortune/0002-h5.git' [branch]='master')
declare -A a03=([version]='0.1.12' [name]='0003' [git]='https://gitlab.com/championgameteam/production/game/nbsoccer/0003-h5.git' [branch]='app-h5')
declare -A a04=([version]='0.1.12' [name]='0004' [git]='https://gitlab.com/championgameteam/production/game/luckymouse/0004-h5.git' [branch]='app-h5')
declare -A a05=([version]='0.1.12' [name]='0005' [git]='https://gitlab.com/championgameteam/production/game/classicfruitbar/0005-h5.git' [branch]='app-h5')
declare -A a06=([version]='0.1.25' [name]='0006' [git]='https://gitlab.com/championgameteam/production/game/diamondcity/0006-h5.git' [branch]='master')
declare -A a07=([version]='0.1.12' [name]='0007' [git]='https://gitlab.com/championgameteam/production/game/scratch/0007-h5.git' [branch]='app-h5')
declare -A a08=([version]='0.1.12' [name]='0008' [git]='https://gitlab.com/championgameteam/production/game/0008_soccerfrenzy/0008-h5.git' [branch]='master')
declare -A a09=([version]='0.1.12' [name]='0009' [git]='https://gitlab.com/championgameteam/production/game/gethigh/0009-h5.git' [branch]='app-h5')
declare -A a10=([version]='0.1.12' [name]='0010' [git]='https://gitlab.com/championgameteam/production/game/fortunekid/0010-h5.git' [branch]='app-h5')
declare -A a11=([version]='0.1.20' [name]='0011' [git]='https://gitlab.com/championgameteam/production/game/moneytree/0011-h5.git' [branch]='master')
declare -A a12=([version]='0.1.12' [name]='0012' [git]='https://gitlab.com/championgameteam/production/game/westjourney/0012-h5.git' [branch]='app-h5')
declare -A a13=([version]='0.1.12' [name]='0013' [git]='https://gitlab.com/championgameteam/production/game/goldenegypt/0013-h5.git' [branch]='app-h5')
declare -A a14=([version]='0.1.12' [name]='0014' [git]='https://gitlab.com/championgameteam/production/game/westjourney_ii/0014-h5.git' [branch]='app-h5')
declare -A a15=([version]='0.1.20' [name]='0015' [git]='https://gitlab.com/championgameteam/production/game/richoxyear/0015-h5.git' [branch]='master')
declare -A a16=([version]='0.1.12' [name]='0016' [git]='https://gitlab.com/championgameteam/production/game/christmastree/0016-h5.git' [branch]='app-h5')
declare -A a17=([version]='0.1.12' [name]='0017' [git]='https://gitlab.com/championgameteam/production/game/diamondslot/0017-h5.git' [branch]='app-h5')
declare -A a18=([version]='0.1.12' [name]='0018' [git]='https://gitlab.com/championgameteam/production/game/shanghaibeauty/0018-h5.git' [branch]='app-h5')
declare -A a19=([version]='0.1.17' [name]='0019' [git]='https://gitlab.com/championgameteam/production/game/mamamia/0019-h5.git' [branch]='master')
declare -A a20=([version]='0.1.22' [name]='0020' [git]='https://gitlab.com/championgameteam/production/game/longlonglong/0020-h5.git' [branch]='master')
declare -A a21=([version]='0.1.12' [name]='0021' [git]='https://gitlab.com/championgameteam/production/game/footballgirl/0021-h5.git' [branch]='app-h5')
declare -A a22=([version]='0.1.29' [name]='0022' [git]='https://gitlab.com/championgameteam/production/game/hotchili/0022-h5.git' [branch]='master')
declare -A a23=([version]='0.1.12' [name]='0023' [git]='https://gitlab.com/championgameteam/production/game/hotDealer/0023-h5.git' [branch]='app-h5')
declare -A a24=([version]='0.1.22' [name]='0024' [git]='https://gitlab.com/championgameteam/production/game/ninja/0024-h5.git' [branch]='master')
declare -A a25=([version]='0.1.12' [name]='0025' [git]='https://gitlab.com/championgameteam/production/game/kingtiger/0025-h5.git' [branch]='app-h5')
declare -A a26=([version]='0.1.12' [name]='0026' [git]='https://gitlab.com/championgameteam/production/game/lottery/0026-h5.git' [branch]='app-h5')
declare -A a27=([version]='0.1.12' [name]='0027' [git]='https://gitlab.com/championgameteam/production/game/bankvault/0027-h5.git' [branch]='app-h5')
declare -A a28=([version]='0.1.12' [name]='0028' [git]='https://gitlab.com/championgameteam/production/game/fortunedragon/0028-h5.git' [branch]='app-h5')
declare -A a29=([version]='0.1.12' [name]='0029' [git]='https://gitlab.com/championgameteam/production/game/mermaid/0029-h5.git' [branch]='app-h5')
declare -A a30=([version]='0.1.3' [name]='0030' [git]='https://gitlab.com/championgameteam/production/game/cowgirl/0030-h5.git' [branch]='master')
declare -A a31=([version]='0.1.0' [name]='0031' [git]='https://gitlab.com/championgameteam/production/game/happysicbo/0031-h5.git' [branch]='master')
declare -A a32=([version]='0.1.8' [name]='0032' [git]='https://gitlab.com/championgameteam/production/game/highlownumber/0032-h5.git' [branch]='master')
declare -A a33=([version]='0.1.7' [name]='0033' [git]='https://gitlab.com/championgameteam/production/game/highlowdice/0033-h5.git' [branch]='master')
declare -A a34=([version]='0.1.0' [name]='0034' [git]='https://gitlab.com/championgameteam/production/game/halloween/0034-h5.git' [branch]='master')

declare -n a

commands=(1 2 3 4)
descriptions=(\
	'git clone'\
    'git common'\
    'build'\
    'build version'\
	)
funcs=(\
	'git_clone'\
    'git_common'\
    'build'\
    'build_version'\
	)

function git_clone {
	for a in ${!a@}; do	
		cd ..
		if [ ! -d ${a[name]} ]; then
			git clone -b ${a[branch]} ${a[git]} ${a[name]}
			cd ${a[name]}
			if [ ${a[name]} != 'lobby-h5' ]; then
				git submodule init
				git submodule update
			fi
			cd ../app
		else
			cd app
		fi
	done
}

function git_common {
	for a in ${!a@}; do
		if [ ${a[name]} != 'lobby-h5' ]; then
			echo -e git common ${a[name]}
			cd ../${a[name]}
			git pull
			cd ./assets/${a[name]}/common
			git reset --hard
			git clean -df
			git pull
			cd ../../..
			git add assets/${a[name]}/common
			git commit -m "mod. submodule"
			git push
			cd ../app
		fi
	done
}

function build {
	if [[ $CC_BIN != "" ]]; then
		for a in ${!a@}; do
			if [ ${a[name]} != 'lobby-h5' ]; then
				echo -e build ${a[name]}
				$CC_BIN --path ../${a[name]} --build "platform=android"
			fi
		done
	else
		echo "Please set CC_BIN = path/CocosCreator"
	fi
}

function build_version {
	for a in ${!a@}; do
		if [ ${a[name]} != 'lobby-h5' ]; then
			echo -e build version ${a[name]}
			if [ ! -d "remote" ]; then
				mkdir ./remote
			fi
			if [ ! -d "remote/"${a[name]} ]; then
				mkdir ./remote/${a[name]}
			fi
			cp -r ../${a[name]}/build/jsb-link/remote/${a[name]}* ./remote/${a[name]}
			node version_generator.js -v ${a[version]} -u base_url/${a[name]}/remote/ -s ./remote/${a[name]}/ -d ./remote/${a[name]}/ -n ${a[name]}
			mkdir ./remote/${a[name]}/remote
			mv ./remote/${a[name]}/${a[name]}* ./remote/${a[name]}/remote
			if [ $QC != 0 ]; then 
				rm -rf ../../../c/xampp/htdocs/${a[name]}
			fi
		fi
	done
	
 	if [ $QC == 0 ]; then 
 		pscp -P 22 -l syadmin -pw richS123zxc -r ./remote/ 10.20.107.31:remote/tmp
 	else
 		mv ./remote/* ../../../c/xampp/htdocs
	fi
	
	rm -rf ./remote
}

function menu {
	echo "======================================"
	echo "  cocos creator tool" 
	echo "  (time: $(date '+%Y-%m-%d %H:%M:%S'))"
	echo "======================================"
	for i in "${!commands[@]}"; do
		echo "  ${commands[$i]}. ${descriptions[$i]}"
	done
	echo "  q. Exit"
	echo "====================================="

	read -p "Please Select: " cmd
	if [[ $cmd == [Qq]* ]]; then
		return 1
	fi
	findCmd=false
	for i in "${!commands[@]}"; do
		if [ $cmd == ${commands[$i]} ]; then
			eval ${funcs[$i]}
			findCmd=true
			break
		fi
	done
	if [ $findCmd == false ]; then
		echo "Please enter valid number or q to exit."
	fi
	if [[ $cmd != [Qq]* ]]; then
		return 1
    fi
	return 0
}

while menu; do
	echo
done
