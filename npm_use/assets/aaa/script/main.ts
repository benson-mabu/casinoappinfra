import { Controller } from "./Controller";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
  @property(cc.Label)
  label: cc.Label = null;

  onLoad() {
    cc.log("hello");
    let data = "123456";
    let controller = new Controller(data);
    controller.onLoad(data);
    this.label.string = controller.md5;
  }
}
