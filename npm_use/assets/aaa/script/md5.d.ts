declare function MD5(
  message: string | Buffer | number[] | Uint8Array,
  options?: Pick<md5.Options, "asString" | "encoding">
): string;
