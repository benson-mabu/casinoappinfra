declare class ActionheroWebsocketClient {
  constructor(options:{url?: string});
  on(event: string, callback: function (message):void);
  connect(callback:function (error, details));
}
