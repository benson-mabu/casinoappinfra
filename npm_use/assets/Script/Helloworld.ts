import { WSHelper } from "./WSHelper";

const { ccclass, property } = cc._decorator;
@ccclass
export default class Helloworld extends cc.Component {
  @property(cc.Label)
  label: cc.Label = null;

  @property(cc.Label)
  hashLabel: cc.Label = null;

  @property
  text: string = "hello";

  wsClient21: ActionheroWebsocketClient;

  wsClientLobby: ActionheroWebsocketClient;

  onRemote() {
    cc.assetManager.loadBundle(
      "http://127.0.0.1:5500/build/jsb-link/remote/aaa",
      (err: Error, bundle: cc.AssetManager.Bundle) => {
        if (!err) {
          bundle.loadScene("main", function (err: Error, scene: cc.Scene) {
            if (!err) {
              cc.director.runScene(scene);
            }
          });
        }
      }
    );
  }

  start() {
    // init logic
    this.label.string = this.text;
    this.hashLabel.string = MD5(this.text);

    // 測試 cc.assetManager.loadRemote
    // ("https://cdn.cg777.net/0021/javascript/ActionheroWebsocketClient.js");

    this.hashLabel.string = "loadRemote";
    const ws21 = new WSHelper(
      "https://cdn.xtnyjx.com/0021/javascript/ActionheroWebsocketClient.js",
      "https://game.mabu666.com/0020/"
    );
    ws21.load((err) => {
      if (err) {
        this.hashLabel.string = "loadRemote Fail";
      } else {
        this.wsClient21 = ws21.client;

        if (this.wsClient21) {
          this.hashLabel.string = "loadRemote Success";
          this.wsClient21.on("connected", () => {
            this.hashLabel.string = "Connected";
          });
          this.wsClient21.on("disconnected", () => {
            this.hashLabel.string = "disconnected";
          });
          this.wsClient21.on("message", (message) => {
            if (message.welcome) {
              this.hashLabel.string = message.welcome;
            }
          });

          cc.log("wsClient21.connect");
          ws21.connect();
        }
      }
    });

    const wsLobby = new WSHelper(
      "https://cdn.xtnyjx.com/mabu/javascript/ActionheroWebsocketClient.js",
      "https://game.mabu666.com/LobbySystem/"
    );
    wsLobby.load((err) => {
      if (err) {
        this.hashLabel.string = "loadRemote Fail";
      } else {
        this.wsClientLobby = wsLobby.client;
        cc.log("wsClientLobby.connect");
        wsLobby.connect();
      }
    });
  }
}
