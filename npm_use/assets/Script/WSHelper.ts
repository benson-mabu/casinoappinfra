// import md5 = require("md5");
export class WSHelper {
  name: string;
  client: ActionheroWebsocketClient;
  url: string;
  wsUrl: string;
  hash: string;
  constructor(url: string, wsUrl: string) {
    this.name = "WSHelper";
    this.url = url;
    this.wsUrl = wsUrl;
    // this.hash = md5(this.url);
  }

  load(callback: (err) => void) {
    const { url, wsUrl } = this;
    cc.log(this.name + "Hash=" + this.hash);
    cc.assetManager.loadRemote(url, (err) => {
      if (err) {
        cc.error(err);
        //   this.hashLabel.string = "loadRemote Fail";
      } else {
        this.client = new ActionheroWebsocketClient({
          url: wsUrl,
        });

        if (this.client) {
          // this.hashLabel.string = "loadRemote Success";
          this.client.on("connected", () => {
            cc.log(this.url + " connected");
            //   this.hashLabel.string = "Connected";
          });
          this.client.on("disconnected", () => {
            cc.log(this.url + " connected");
            //   this.hashLabel.string = "disconnected";
          });
          this.client.on("error", (error) => {
            cc.error(this.url + " error", error.stack);
          });
          this.client.on("reconnect", () => {
            cc.log(this.url + " reconnect");
          });
          this.client.on("reconnecting", () => {
            cc.log(this.url + " reconnecting");
          });

          this.client.on("message", (message) => {
            cc.log(this.url + "message", message);
            //   if (message.welcome) {
            //     this.hashLabel.string = message.welcome;
            //   }
          });
        }
      }
      return callback(err);
    });
  }

  connect() {
    cc.log(this.url + ":connect()");
    this.client.connect((error, details) => {
      if (error) {
        cc.warn(this.url + details);
      } else {
        cc.log(this.url + " OK");
      }
    });
  }
}
