package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"github.com/gofrs/uuid"
)

const (
	FileRead = iota
	FileWrite
)

type ID struct {
	OldUuid string
	OldType string
	NewUuid string
	NewType string
}

var uuids map[string]interface{}
var tmp map[string][]ID

func StringsBuilder(a ...string) string {
	builder := strings.Builder{}

	builder.Reset()
	for _, s := range a {
		builder.WriteString(s)
	}

	return builder.String()
}

func CompressUuid(s string) string {
	base64Chars := ""
	Base64KeyChars := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

	s = strings.ReplaceAll(s, "-", "")
	i := 5
	for {
		if i >= len(s) {
			break
		}

		hexVal1, _ := strconv.ParseInt(string(s[i]), 16, 64)
		hexVal2, _ := strconv.ParseInt(string(s[i+1]), 16, 64)
		hexVal3, _ := strconv.ParseInt(string(s[i+2]), 16, 64)

		base64Chars = StringsBuilder(base64Chars, string(Base64KeyChars[(hexVal1<<2)|(hexVal2>>2)]), string(Base64KeyChars[((hexVal2&3)<<4)|hexVal3]))

		i += 3
	}

	return StringsBuilder(s[0:5], base64Chars)
}

func checkUuid(dir string, pathname string, s string) {
	ok := func() bool {
		if uuids[s] == nil {
			uuids[s] = pathname
			return false
		}
		return true
	}()

	if ok {
		if !strings.Contains(pathname, "common") {
			id := ID{}
			for {
				ok = func() bool {
					uuidv4, _ := uuid.NewV4()
					if uuids[uuidv4.String()] == nil {
						uuids[uuidv4.String()] = pathname
						id.NewUuid = uuidv4.String()
						return true
					}
					return false
				}()

				if ok {
					break
				}
			}
			id.OldUuid = s
			id.OldType = CompressUuid(id.OldUuid)
			id.NewType = CompressUuid(id.NewUuid)

			if tmp[dir] == nil {
				tmp[dir] = []ID{}
			}

			tmp[dir] = append(tmp[dir], id)
		}
	}
}

func RW(dir string, path string, status uint8) {
	files, _ := ioutil.ReadDir(path)
	for _, file := range files {
		pathname := StringsBuilder(path, "/", file.Name())
		context, err := ioutil.ReadFile(pathname)
		if err != nil {
			RW(dir, pathname, status)
		} else {
			if status == FileWrite {
				if !strings.Contains(pathname, "common") {
					if strings.Contains(pathname, ".meta") || strings.Contains(pathname, ".fire") || strings.Contains(pathname, ".prefab") {
						t := string(context)
						for _, id := range tmp[dir] {
							t = strings.ReplaceAll(strings.ReplaceAll(t, id.OldUuid, id.NewUuid), id.OldType, id.NewType)
						}
						err = ioutil.WriteFile(pathname, []byte(t), 0766)
						if err != nil {
							fmt.Println(err)
							os.Exit(1)
						} else {
							fmt.Println("replace:", pathname)
						}
					}
				}
			} else {
				if strings.Contains(pathname, ".meta") {
					var object map[string]interface{}
					err := json.Unmarshal(context, &object)
					if err != nil {
						fmt.Println(err)
						os.Exit(1)
					}

					for key, value := range object {
						if key == "uuid" {
							checkUuid(dir, pathname, value.(string))
						}
						if key == "subMetas" {
							if subMetas, ok := value.(map[string]interface{}); ok {
								for _, subMetasValue := range subMetas {
									if filename, ok := subMetasValue.(map[string]interface{}); ok {
										checkUuid(dir, pathname, filename["uuid"].(string))
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

func main() {
	uuids = make(map[string]interface{})
	tmp = make(map[string][]ID)
	dirs := []string{
		"common",
		"app",
		"lobby",
		"lobby-h5",
		"0002",
		"0003",
		"0004",
		"0005",
		"0007",
		"0009",
		"0010",
		"0012",
		"0013",
		"0014",
		"0017",
		"0018",
		"0021",
		"0023",
		"0025",
		"0026",
		"0027",
		"0028",
		"0029",
		"0006",
		"0008",
		"0011",
		"0015",
		"0016",
		"0019",
		"0020",
		"0022",
		"0024",
		"0030",
		"0031",
		"0032",
		"0033",
	}
	for _, dir := range dirs {
		fmt.Println("run dir:", dir)
		var path string
		switch dir {
		case "common", "json":
			path = StringsBuilder("../../", dir, "")
		case "app":
			path = StringsBuilder("../../", dir, "/app/assets")
		default:
			path = StringsBuilder("../../", dir, "/assets")
		}
		RW(dir, path, FileRead)
		if tmp[dir] != nil {
			RW(dir, path, FileWrite)
		}
	}
}
